package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;

import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * Représente la tâche Recherche qu'on passe en paramètre d'un Thread, pour lancer la recherche dans un thread a part
 * @author Motyak
 * @param <V>
 *
 */
public class Task_Recherche<V> extends TaskWrapper<V> {
	
	private Button btnRecherche;
	private Button btnArret;
	private ComboBox<String> cbVille;
	private TextArea txtaItineraire;
	private Pane panelControles;
	
	//pour pouvoir rebind le bouton après l'avoir disable
	private BooleanBinding bb;
	
	Task_Recherche(Button btnRecherche,Button btnArret,ComboBox<String> cbVille,TextArea txtaItineraire,BooleanBinding bb,Pane panelControles)
	{
		this.btnRecherche=btnRecherche;
		this.btnArret=btnArret;
		this.cbVille=cbVille;
		this.txtaItineraire=txtaItineraire;
		this.bb=bb;
		this.panelControles=panelControles;
	}
	
	@Override
	protected V call() {
		Launcher.taskHandler.setRunning(true);
		this.updateMessage("Recherche en cours...");
		
		try {
			String osmReq=Controleur.generateOSMRequest(this.cbVille.getValue().toString());
			if(osmReq==null)
				return null;
			String resFile=Controleur.sendReqAndCacheRes(osmReq);
			if(resFile==null)
				return null;
			//afficher les 10 premieres et 10 dernieres lignes du fichier cache dans la text area
			this.txtaItineraire.setText(PersiFichierText.persiOverview(resFile, 10));
			//la partie parsing
//			Parser.defineRacine(resFile);
//			ArrayList<String> highways=Parser.getHighway();
//			for(String h : highways)
//			{
//				if(Launcher.taskHandler!=null && Launcher.taskHandler.getTask()!=null && Launcher.taskHandler.getTask().isCancelled())
//				{
//					Launcher.taskHandler.getTask().updateMessageFromOut("Recherche annulée.");
//					Launcher.taskHandler.setRunning(false);
//					return null;
//				}
//				System.out.println("Nom way : "+Parser.getNameWayById(h));
//				System.out.println("Coords way : ");
//				System.out.println(Parser.getCoordWayById(h));
//				System.out.println("\n\n");
//			}
		} catch (Exception e) {
			e.printStackTrace();
			//mettre un message d'erreur avec le nom de l'exception et un descriptif
			this.updateMessage("Erreur de type "+e.getClass()+" : "+e.getMessage());
		}
		finally {
			
			//tout activer sauf bouton 'annuler'
			Launcher.griserElements(this.panelControles.getChildren(), false);
			this.btnArret.setDisable(true);
			this.btnRecherche.disableProperty().bind(this.bb);
			
			Launcher.taskHandler.setRunning(false);
			
		}
		
		this.updateMessage("Recherche réussie !");
		
		return null;
	}
}
