package fr.univavignon.ceri.deskmap;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Les méthodes de cette classes sont invoquées dans le Launcher
 * @author Anfosso Tommy
 */
public class Controleur {
	
	static public final String OVERPASS_API_URL="https://lz4.overpass-api.de/api/interpreter";
	
	/**
	 * Genere la requete OSM xml
	 * @param ville
	 * @return
	 * @throws MalformedURLException 
	 * @throws IOException
	 */
	static String generateOSMRequest(String ville) throws IOException 
	{
		return OSMRequest.get(ville, RelType.ROAD);
	}
	
	/**
	 * Envoi de la requete HTTP à l'API + stockage de la réponse dans le Cache
	 * @param req
	 * @return
	 * @throws Exception
	 */
	static String sendReqAndCacheRes(String req) throws Exception
	{
		//si req est null => leve une exception
		if(req==null)
			throw new Exception("req is null");
		
		//calculer le hash MD5 de req et l'assigner a une variable locale
		String hash=MD5.toString(req);
		
		//si aucun fichier porte ce nom dans le Cache
		if(!Cache.exists(hash))
		{
			//appeler HttpCon.request(..) et assigner resultat a un String
			String res=HttpCon.request(Type.POST,
				Controleur.OVERPASS_API_URL,
				new String[] {"Content-Type: text/xml", "Accept: text/xml"},
				req
			);
			
			
			if(res==null)
				return null;
			//ecrire resultat dans un fichier texte portant comme nom le hash du res
			PersiFichierText.stringToPersi(res, Cache.DIR_PATH + hash);
			//ajouter fichier au Cache
			Cache.add(hash);	
		}
		return Cache.DIR_PATH + hash;
	}

	
	
//	//methode appelée au lancement de l'appli
//	public void initialize()
//	{
//	//griser les éléments dont je n'ai pas besoin
//		this.griserElements(this.panelControles.getChildren(), true);
//		this.griserElements(Arrays.asList(
//			this.lblVille,
//			this.txtVille,
//			this.btnVille,
//			this.txtaItineraire,
//			this.btnRechercher
//		), false);
//		
//		//verifie si le répertoire .cache existe, sinon le creer
//		File file=new File(System.getProperty("user.dir")+File.separator+".cache");
//		if(!file.exists())
//			file.mkdir();
//	}
	
	
}
