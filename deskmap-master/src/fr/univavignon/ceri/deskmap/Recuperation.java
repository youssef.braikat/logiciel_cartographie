package fr.univavignon.ceri.deskmap;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.*;
import java.nio.charset.StandardCharsets;

/***
 * @author Antoine Olivero
 *
 */

public class Recuperation {

	/*
	(
   	node(43.9422124291,4.7976846362,43.9540771394,4.8200435305);
   	<;
	);
	out meta;
	*/

    /**
     * Permet de créer un fichier contenant toute les villes de france
     * url de l'API : https://lz4.overpass-api.de/api/interpreter
     * Permet de stocket la reponse soit en XML soit en Json
     * @throws IOException
     */
    void RecupVille() throws IOException {
    	
    	File file = new File(System.getProperty("user.dir")+File.separator+"Listeville.xml");
    	//File file = new File(System.getProperty("user.dir")+File.separator+"Listeville.json");
    	
        if (file.exists()) {

        }
        else {

            file.createNewFile();

            // creation de l'objet URL
            URL url = new URL("https://lz4.overpass-api.de/api/interpreter");
            System.out.println("creation de l'objet URL");

            // connection à l'objet URL
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Content-Type", "text/xml");
            httpURLConnection.setDoOutput(true);
            System.out.println("connection etablie");

            // envoie requette

            //requete json avec retour json : plus court et plus simple -> retour plus rapide
            //String request = [out:json];{{geocodeArea:France}}->.searchArea;(node['place'='city'](area.searchArea);node['place'='town'](area.searchArea););out meta;>;out tags;

            //requete xml avec retour xml -> utile pour l'insant car le parseur fonctionne avec de l'xml
            String request = "<osm-script output=\"xml\" output-config=\"\">\n" +
                    "  <id-query type=\"area\" ref=\"3602202162\" into=\"searchArea\"/>\n" +
                    "  <union into=\"_\">\n" +
                    "    <query into=\"_\" type=\"node\">\n" +
                    "      <has-kv k=\"place\" modv=\"\" v=\"city\"/>\n" +
                    "      <area-query from=\"searchArea\"/>\n" +
                    "    </query>\n" +
                    "    <query into=\"_\" type=\"node\">\n" +
                    "      <has-kv k=\"place\" modv=\"\" v=\"town\"/>\n" +
                    "      <area-query from=\"searchArea\"/>\n" +
                    "    </query>\n" +
                    "  </union>\n" +
                    "  <print e=\"\" from=\"_\" geometry=\"skeleton\" ids=\"yes\" limit=\"\" mode=\"meta\" n=\"\" order=\"id\" s=\"\" w=\"\"/>\n" +
                    "  <recurse from=\"_\" into=\"_\" type=\"down\"/>\n" +
                    "  <print e=\"\" from=\"_\" geometry=\"skeleton\" ids=\"yes\" limit=\"\" mode=\"tags\" n=\"\" order=\"id\" s=\"\" w=\"\"/>\n" +
                    "</osm-script>";
            System.out.println(request);

            OutputStream outputStream = httpURLConnection.getOutputStream();
            byte[] data = request.getBytes(StandardCharsets.UTF_8);
            outputStream.write(data);
            System.out.println("Envoie de la requette");
            outputStream.flush();
            outputStream.close();
            System.out.println("connection d'envoie fini");

            // retour API
            StringBuilder reponse;
            try (BufferedReader input = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()))) {

                String line;
                reponse = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    reponse.append(line);
                    reponse.append(System.lineSeparator());
                }

                System.out.println(reponse);

                //ecrit le retour de l'API dans le fichier ListeVille.json
                //BufferedWriter bufferedWriter = new BufferedWriter((new FileWriter(System.getProperty("user.dir")+File.separator+"Listeville.json")));

                //ecrit le retour de l'API dans le fichier ListVille.xml
                BufferedWriter bufferedWriter = new BufferedWriter((new FileWriter(System.getProperty("user.dir")+File.separator+"Listeville.xml")));

                bufferedWriter.write(String.valueOf(reponse));
                bufferedWriter.close();

                } finally {
                    httpURLConnection.disconnect();
                }
        }
    }


    /**
     * Permet de créer un fichier contenant les route d'une ville.
     * On doit preciser dans la requête quel "type" de route l'on souhaite
     * url de l'API : https://lz4.overpass-api.de/api/interpreter
     * Permet de stocket la reponse soit en XML soit en Json
     * @throws IOException
     */
    public void RecupRoute() throws IOException{

    	File file = new File(System.getProperty("user.dir")+File.separator+"ListeRue.json");
    	//File file = new File(System.getProperty("user.dir")+File.separator+"ListeRue.xml");

    	// creation de l'objet URL
        URL url = new URL("https://lz4.overpass-api.de/api/interpreter");
        System.out.println("creation de l'objet URL");

        // connection à l'objet URL
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("Content-Type", "text/xml");
        httpURLConnection.setDoOutput(true);
        System.out.println("connection etablie");

        // envoie requette

        //requete json avec retour json : plus court et plus simple -> retour plus rapide
         /*String request = "[out:xml][timeout:25];
	        {{geocodeArea:Avignon}}->.searchArea;
	        (
	          node["highway"="primary"](area.searchArea);
	          way["highway"="primary"](area.searchArea);
	          relation["highway"="primary"](area.searchArea);

	          node["highway"="secondary"](area.searchArea);
	          way["highway"="secondary"](area.searchArea);
	          relation["highway"="secondary"](area.searchArea);

	          node["highway"="tertiary"](area.searchArea);
	          way["highway"="tertiary"](area.searchArea);
	          relation["highway"="tertiary"](area.searchArea);

	          node["highway"="residential"](area.searchArea);
	          way["highway"="residential"](area.searchArea);
	          relation["highway"="residential"](area.searchArea);

	          node["highway"="unclassified"](area.searchArea);
	          way["highway"="unclassified"](area.searchArea);
	          relation["highway"="unclassified"](area.searchArea);

              node["highway"="pedestrian"](area.searchArea);
              way["highway"="pedestrian"](area.searchArea);
	          relation["highway"="pedestrian"](area.searchArea);

              node["highway"="living_street"](area.searchArea);
              way["highway"="living_street"](area.searchArea);
	          relation["highway"="living_street"](area.searchArea);

	        );
	        out body;
	        >;
	        out skel qt;
	        */



        //requete xml avec retour xml -> utile pour l'insant car le parseur fonctionne avec de l'xml
        String request = "<osm-script output=\"json\" output-config=\"\" timeout=\"25\">\n" +
                "  <id-query type=\"area\" ref=\"3600102478\" into=\"searchArea\"/>\n" +
                "  <union into=\"_\">\n" +
                "    <query into=\"_\" type=\"node\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"primary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"way\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"primary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"relation\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"primary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"node\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"secondary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"way\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"secondary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"relation\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"secondary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"node\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"tertiary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"way\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"tertiary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"relation\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"tertiary\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"node\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"residential\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"way\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"residential\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"relation\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"residential\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"node\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"unclassified\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"way\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"unclassified\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "    <query into=\"_\" type=\"relation\">\n" +
                "      <has-kv k=\"highway\" modv=\"\" v=\"unclassified\"/>\n" +
                "      <area-query from=\"searchArea\"/>\n" +
                "    </query>\n" +
                "  </union>\n" +
                "  <print e=\"\" from=\"_\" geometry=\"skeleton\" ids=\"yes\" limit=\"\" mode=\"body\" n=\"\" order=\"id\" s=\"\" w=\"\"/>\n" +
                "  <recurse from=\"_\" into=\"_\" type=\"down\"/>\n" +
                "  <print e=\"\" from=\"_\" geometry=\"skeleton\" ids=\"yes\" limit=\"\" mode=\"skeleton\" n=\"\" order=\"quadtile\" s=\"\" w=\"\"/>\n" +
                "</osm-script>";


        System.out.println(request);

        OutputStream outputStream = httpURLConnection.getOutputStream();
        byte[] data = request.getBytes(StandardCharsets.UTF_8);
        outputStream.write(data);
        System.out.println("Envoie de la requette");
        outputStream.flush();
        outputStream.close();
        System.out.println("connection d'envoie fini");

        // retour API
        StringBuilder reponse;
        try (BufferedReader input = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()))) {
            String line;
            reponse = new StringBuilder();
            while ((line = input.readLine()) != null) {
                reponse.append(line);
                reponse.append(System.lineSeparator());
            }
            System.out.println(reponse);

            //ecrit le retour de l'API dans le fichier ListeRue.json
            BufferedWriter bufferedWriter = new BufferedWriter((new FileWriter(System.getProperty("user.dir")+File.separator+"ListeRue.json")));

            //ecrit le retour de l'API dans le fichier ListeRuee.xml
            //BufferedWriter bufferedWriter = new BufferedWriter((new FileWriter(System.getProperty("user.dir")+File.separator+"ListeRue.xml")));

            bufferedWriter.write(String.valueOf(reponse));
            bufferedWriter.close();
        } finally {
            httpURLConnection.disconnect();
        }
    }


    /**
     * Permet de créer un Bbox qui qui nous rennvera toute les information contenu en elle
     * La Bbox est générée au tour du Noued de la ville.
     * Retourne toute les information dans le fichier Bbox.xml
     *
     * Les coordonnées de la ville sont pour l'instant donné à la main.
     * Objectif final -> passer les coordonnées en argument de la methode.
     * -> se fait une fois l'autocompletion finis
     *
     * @throws IOException
     */
    void CreateBbox(double lat, double longit) throws IOException {

        //File file = new File(System.getProperty("user.dir")+File.separator+"Bbox.json");
        File file = new File(System.getProperty("user.dir")+File.separator+"Bbox.xml");

        //latidue de la ville que l'on aura choisie d'afficher dans l'interface
        BigDecimal latitude = new BigDecimal(lat);
        //longitude de la ville que l'on aura choisie d'afficher dans l'interface
        BigDecimal longitude = new BigDecimal( longit);

        MathContext mclat = new MathContext(8);
        MathContext mclong = new MathContext(7);
        BigDecimal scaleBbox = new BigDecimal(0.01000);

        //creation de la bbox
        BigDecimal latPointInfGauche = latitude.subtract(scaleBbox, mclat);
        BigDecimal longPointInfGauche = longitude.subtract(scaleBbox, mclong);
        BigDecimal latPointSupDroit = latitude.add(scaleBbox, mclat);
        BigDecimal longPointSupDroit = longitude.add(scaleBbox, mclong);

        //requete au format overpassQL return du xml
        //String requeteBbox = "(node("+latPointInfGauche+","+longPointInfGauche+","+latPointSupDroit+","+longPointSupDroit+");<;);out meta;";
        //requete au format xml return du xml
        String requeteBbox =    "<osm-script>\n" +
                                "  <union into=\"_\">\n" +
                                "    <bbox-query s=\""+latPointInfGauche+"\" w=\""+longPointInfGauche+"\" n=\""+latPointSupDroit+"\" e=\""+longPointSupDroit+"\"/>\n" +
                                "    <recurse from=\"_\" into=\"_\" type=\"up\"/>\n" +
                                "  </union>\n" +
                                "  <print e=\"\" from=\"_\" geometry=\"skeleton\" ids=\"yes\" limit=\"\" mode=\"meta\" n=\"\" order=\"id\" s=\"\" w=\"\"/>\n" +
                                "</osm-script>";

        if(file.exists()) {
            System.out.println("le fichier existe");

            // creation de l'objet URL
            URL url = new URL("https://lz4.overpass-api.de/api/interpreter");
            System.out.println("creation de l'objet URL");

            // connection à l'objet URL
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Content-Type", "text/xml");
            httpURLConnection.setDoOutput(true);
            System.out.println("connection etablie");

            System.out.println(requeteBbox);

            OutputStream outputStream = httpURLConnection.getOutputStream();
            byte[] data = requeteBbox.getBytes(StandardCharsets.UTF_8);
            outputStream.write(data);
            System.out.println("Envoie de la requette");
            outputStream.flush();
            outputStream.close();
            System.out.println("connection d'envoie fini");

            // retour API
            StringBuilder reponse;
            try (BufferedReader input = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()))) {
                String line;
                reponse = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    reponse.append(line);
                    reponse.append(System.lineSeparator());
                }

                //ecrit le retour de l'API dans le fichier Bbox.xml
                BufferedWriter bufferedWriter = new BufferedWriter((new FileWriter(System.getProperty("user.dir")+File.separator+"Bbox.xml")));

                bufferedWriter.write(String.valueOf(reponse));
                bufferedWriter.close();
            } finally {
                httpURLConnection.disconnect();
            }

        }
        else {
            file.createNewFile();
            System.out.println("le fichier viens d'étre créé");
            CreateBbox( lat, longit);
        }
    }
}
