package fr.univavignon.ceri.deskmap;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * @author Motyak
 *
 */
public class Cache {

	/**
	 * représente le chemin absolu du dossier .cache
	 */
	static public final String DIR_PATH = System.getProperty("user.dir") + File.separator + ".cache" + File.separator;
	
	/**
	 * représente l'ensemble des chemins absolus des fichiers enregistrés dans le Cache
	 */
	static private ArrayList<String> files = new ArrayList<String>();
	
	public Cache() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * retourne vrai si le fichier est enregistré dans le Cache ET qu'il existe physiquement
	 * @param fileName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public static boolean exists(String fileName) throws NoSuchAlgorithmException, IOException
	{
		//verifier que le fichier est dans le cache et qu'il existe physiquement
		File file=new File(Cache.DIR_PATH + fileName);
		return (Cache.files.contains(Cache.DIR_PATH+fileName) && file.exists());
	}
	
	/**
	 * Permet d'ajouter un fichier, qui existe déjà physiquement, dans le Cache
	 * @param fileName
	 * @throws Exception
	 */
	public static void add(String fileName) throws Exception
	{
		//si le fichier n'existe pas..
		if(!new File(Cache.DIR_PATH + fileName).exists())
			throw new Exception("Le fichier à ajouter au Cache n'existe pas physiquement");
		
		//si le fichier n'est pas déjà dans le cache..
		if(!Cache.files.contains(Cache.DIR_PATH+fileName))
			Cache.files.add(Cache.DIR_PATH + fileName);
		else
			System.out.println("Le fichier se trouve déjà dans le cache");
	}
	
	/**
	 * Permet de nettoyer tous les fichiers se trouvant dans le dossier du Cache
	 */
	public static void clear()
	{
		//supprimer tous les fichiers se trouvant dans Cache.DIR_PATH
		File dir=new File(Cache.DIR_PATH);
		for(File file: dir.listFiles())
			file.delete();
		Cache.files.clear();
	}
	
	/**
	 * TU de la classe
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		Cache.clear();
		System.out.println(Cache.DIR_PATH);
		String req=new String("test1\ntest2");
		String hash=MD5.toString(req);
		PersiFichierText.stringToPersi("resultat\nde\nla\nrequete", Cache.DIR_PATH + hash);
		Cache.add(hash);
		Cache.add(hash);
		//Affichage liste fichiers du cache
		for(String s : Cache.files)
			System.out.println(s);
	}
}
