package fr.univavignon.ceri.deskmap;

public enum RelType {
	MULTIPOLYGON(0),ROAD(1),MP_OR_ROAD(2);
	
	private final int value;

	private RelType(int value) {
		this.value=value;
	}
	
	public int getValue() {
		return this.value;
	}
}
