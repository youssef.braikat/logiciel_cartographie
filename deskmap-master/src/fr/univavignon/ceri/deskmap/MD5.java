package fr.univavignon.ceri.deskmap;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Motyak
 *
 */
public class MD5 {
	
	public MD5() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * retourne le hash md5 du string en entrée
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String toString(String input) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] messageDigest = md.digest(input.getBytes());
		BigInteger no = new BigInteger(1, messageDigest);
		String hashtext = no.toString(16);
		while (hashtext.length() < 32) { 
			hashtext = "0" + hashtext; 
        } 
        return hashtext; 
	}
	
	/**
	 * TU de la classe
	 * @param args
	 * @throws NoSuchAlgorithmException
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException
	{
		String s = "test1";
		System.out.println("Texte : " + s);
		System.out.println("MD5 : " + MD5.toString(s));
	}
}
