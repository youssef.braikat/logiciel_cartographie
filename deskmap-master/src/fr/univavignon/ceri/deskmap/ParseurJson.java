package fr.univavignon.ceri.deskmap;

import javax.json.*;
import javax.json.stream.*;
import javax.json.stream.JsonParser.*;

import java.io.*;

import java.util.*;

import javafx.collections.*;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

/**
 * @author Thibaud CHEVILLARD
 */
public class ParseurJson
{
	/**
	 * la fonction parcours le fichier ListeVille.json pour trouver les nom
	 * 
	 * @return la liste des nom des villes
	 */
	public static SortedList<String> getCityName()
	{
		JsonParser parser;

		ObservableList<String> items = FXCollections.observableArrayList();
		boolean pass = false;
		try
		{
			parser = Json.createParser(new FileReader("ListeVille.json"));
			
			while (parser.hasNext())
			{
				Event event = parser.next();
				if (event == JsonParser.Event.KEY_NAME )
				{
					String key = parser.getString();
					event = parser.next();
					if (key.equals("name"))
					{
						 String name = parser.getString();
						 items.add(name);
						 pass = true;
					}
			    }
			}
			if(pass == false)
			{
				return null;
			}
//			FilteredList<String> ret = new FilteredList<String>(items, p->true);
//			return ret;
			return items.sorted();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * la fonction parcours le fichier ListeRuee.json pour trouver les nom
	 * 
	 * @return la liste des nom des rues
	 */
	public static FilteredList<String> getWayName()
	{
		JsonParser parser;
		ObservableList<String> items = FXCollections.observableArrayList();
		boolean pass = false;
		try
		{
			parser = Json.createParser(new FileReader("ListeRue.json"));
			
			while (parser.hasNext())
			{
				Event event = parser.next();
				if (event == JsonParser.Event.KEY_NAME )
				{
					String key = parser.getString();
					event = parser.next();
					if (key.equals("name"))
					{
						 String name = parser.getString();
						 items.add(name);
						 pass = true;
					}
			    }
			}
			if(pass == false)
			{
				return null;
			}
			FilteredList<String> ret = new FilteredList<String>(items, p->true);
			return ret;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> getCoordVille(String cityName)
	{
		JsonParser parser;
		try
		{
			parser = Json.createParser(new FileReader("ListeVille.json"));
			
			String lat = null;
			String lon = null;
			ArrayList<String> ret = new ArrayList<String>();
						
			while (parser.hasNext())
			{
				Event event = parser.next();
				if (event == JsonParser.Event.KEY_NAME )
				{
					String key = parser.getString();
					event = parser.next();
					if(key.equals("lat"))
					{
						lat = parser.getString();
					}
					if(key.equals("lon"))
					{
						lon = parser.getString();
					}
					if(key.equals("name"))
					{
						String name = parser.getString();

						if(name.equals(cityName))
						{
							ret.add(lat);
							ret.add(lon);
							return ret;
						}
					}
			    }
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> defineBBox(ArrayList<String> coordVille)
	{
		ArrayList<String> BBox = new ArrayList<String>();
		double lat = Double.valueOf(coordVille.get(0));
		double lon = Double.valueOf(coordVille.get(1));
		
		BBox.add(String.valueOf(lat-0.005));//South
		BBox.add(String.valueOf(lon-0.005));//West
		BBox.add(String.valueOf(lat+0.005));//North
		BBox.add(String.valueOf(lon+0.005));//East
		
		return BBox;
	}
	
	/**
	 * Fonction de test
	 * affiche le contenu d'un FilteredList<String> rentré en parametre.
	 * 
	 * @param t1 FilteredList<String>
	 */
	public static void affFS(FilteredList<String> t1)
	{
		if(t1 != null)
		{
			for(int i = 0; i < t1.size(); i++)
			{
				System.out.println(t1.get(i));
			}
		}
	}
		
	/**
	 * Fonction de test
	 * affiche le contenu d'un ArrayList<String> rentré en parametre.
	 * 
	 * @param t1 ArrayList<String>
	 */
	public static void affAS(ArrayList<String> t1)
	{
		if(t1 != null)
		{
			for(int i = 0; i < t1.size(); i++)
			{
				System.out.println(t1.get(i));
			}
		}
	}
	
	/**
	 * Main servant pour les tests
	 * 
	 * @param args les arguments potentiels
	 */
	public static void main(String[] args)
	{
		//affFS(getCityName());
		//affFS(getWayName());
		ArrayList<String> coord = getCoordVille("Avignon");
		affAS(coord);
		
		System.out.println("---------------------");
		
		ArrayList<String> BBox = defineBBox(coord);	
		affAS(BBox);
	}
}