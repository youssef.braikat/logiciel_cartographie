package fr.univavignon.ceri.deskmap;

public enum Type {
	GET(0),POST(1);
	
	private final int value;
	
	private Type(int value) {
		this.value=value;
	}
	
	public int getValue() {
		return this.value;
	}
}
