package fr.univavignon.ceri.deskmap;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.math.MathContext;


/**
 * @author Antoine Olivero
 */

public class Map extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Drawing Operations Test");
        Group root = new Group();
        Canvas canvas = new Canvas(500, 500);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        //drawShapes(gc);
        ConvertisseurLatLongInXY(gc);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private void drawShapes(GraphicsContext gc) {
        gc.setStroke(Color.GREEN);
        gc.setLineWidth(1);
        gc.strokeLine(40, 10, 10, 40);

        gc.strokePolygon(new double[]{3, 297, 297, 3},
                new double[]{3, 3, 247, 247}, 4);
        gc.strokePolyline(new double[]{110, 140, 110, 140},
                new double[]{210, 210, 240, 240}, 4);
    }

    public static void ConvertisseurLatLongInXY(GraphicsContext gc) {
        //longitude
        double latitudeCentreDAvignon = 43.9492493;
        double longitudeCentreDAvignon = 4.8059012;


        //1° long = OneDegLongitudeInMetre
        double scaleBbox = 0.01000;
        double OneDegLongitudeInMetre = (111110 * Math.cos((scaleBbox) * Math.PI / 180));
        //1° lat = 111110m
        double OneDegLatitudeInMetre = 111110;
        System.out.println(OneDegLatitudeInMetre+" - "+OneDegLongitudeInMetre);

        //Rapport bbox
        //lat->width in metre
        double widthBbox = (2*scaleBbox) * OneDegLatitudeInMetre;
        System.out.println("la largeur de la bbox est égal à " + widthBbox + "m");
        //width in pixel
        double widthPixelBbox = 500/widthBbox;
        System.out.println("1m est égal à " + widthPixelBbox + "px");
        System.out.println(widthBbox*widthPixelBbox);

        //long ->height in metre
        double heightBbox = (2 * scaleBbox) * OneDegLongitudeInMetre;
        System.out.println("la hauteur de la bbox est égal à " + heightBbox + "m");
        //height in pixel
        double heightPixelBbox = 500/heightBbox;
        System.out.println("1m est égal à " + heightPixelBbox + "px");
        System.out.println(heightBbox * heightPixelBbox);

                    //   1            2          3          4            5            6                7      8           9           10           11         12          13            14         15         16           17         18          19          20          21          22          23           24            25        26            27           28      29         30           31            32
        double lat[] = {43.9518813, 43.9522157, 43.9522837, 43.9527102, 43.9527915, 43.9529228, 43.9530270, 43.9531758, 43.9532750, 43.9534822, 43.9535446, 43.9535911, 43.9535645, 43.9535861, 43.9535745, 43.9535996, 43.9535668, 43.9535147, 43.9533590, 43.9532234, 43.9531982, 43.9529377, 43.9526017, 43.9524577, 43.9522286, 43.9519465, 43.9519500, 43.9518955, 43.9517888, 43.9517330, 43.9518172, 43.9518813};
        for (int i = 0; i < 32; i++) {
            lat[i] = (latitudeCentreDAvignon + scaleBbox) - lat[i];
        }
        for (int i = 0; i < 32; i++) {
            lat[i] = lat[i] * widthBbox;
        }
        for (int i = 0; i < 32; i++) {
            lat[i]=lat[i] / widthPixelBbox;
            System.out.println(lat[i]);
        }

        double longit[] = {4.8071723, 4.8070679, 4.8069994, 4.8070146, 4.8070661, 4.8072405, 4.8072351, 4.8073113, 4.8072798, 4.8072136, 4.8073745, 4.8074073, 4.8074952, 4.8077088, 4.8077544, 4.8078778, 4.8079527, 4.8080815, 4.8086004, 4.8091272, 4.8091855, 4.8090905, 4.8087755, 4.8087421, 4.8086151, 4.8084754, 4.8084430, 4.8083469, 4.8083309, 4.8072194, 4.8071925, 4.8071723};
        for (int i = 0; i < 32; i++) {
            longit[i] = (longitudeCentreDAvignon + scaleBbox) - longit[i];
        }
        for (int i = 0; i < 32; i++) {
            longit[i] = longit[i] * heightBbox;
        }
        for (int i = 0; i < 32; i++) {
            longit[i] = longit[i] / heightPixelBbox;
            System.out.println(longit[i]);
        }

        gc.setStroke(Color.GREEN);
        gc.setLineWidth(1);

        gc.strokePolyline(new double[]{lat[0],lat[1],lat[2],lat[3],lat[4],lat[5],lat[6],lat[7],lat[8],lat[9],lat[10],lat[11],lat[12],lat[13],lat[14],lat[15],lat[16],lat[17],lat[18],lat[19],lat[20],lat[21],lat[22],lat[23],lat[24],lat[25],lat[26],lat[27],lat[28],lat[29],lat[30],lat[31]},
                new double[]{longit[0],longit[1],longit[2],longit[3],longit[4],longit[5],longit[6],longit[7],longit[8],longit[9],longit[10],longit[11],longit[12],longit[13],longit[14],longit[15],longit[16],longit[17],longit[18],longit[19],longit[20],longit[21],longit[22],longit[23],longit[24],longit[25],longit[26],longit[27],longit[28],longit[29],longit[30],longit[31]},
                32);
    }
}

