/**
 * 
 */
package fr.univavignon.ceri.deskmap;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author Motyak
 *
 */
public class OSMRequest {
	
	public OSMRequest() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Recupere la bbox d'une ville et retourne la ligne format OSM xml
	 * @param ville
	 * @return
	 * @throws MalformedURLException 
	 * @throws IOException
	 */
	private static String getBboxFromVille(String ville) throws IOException
	{
		//verifier si ville existe, si ce n'est pas le cas retourner null
		String xml=HttpCon.request(Type.GET, 
			"https://nominatim.openstreetmap.org/search?city="+ville+"&countrycodes=fr&limit=1&format=xml", 
			null, 
			null
		);
		if(xml==null)
			return null;
		String xml2=xml.substring(xml.indexOf("boundingbox=\""));
		String xml3=xml2.substring(13, xml2.indexOf("lat='")-2);
		String[] bbox=xml3.split(",");
		
		return "<bbox-query s=\""+bbox[0]+"\" w=\""+bbox[2]+"\" n=\""+bbox[1]+"\" e=\""+bbox[3]+"\"/>\n";
	}

	/**
	 * Genere la requete OSM xml sous la forme d'un String
	 * @param ville
	 * @param rt
	 * @return
	 * @throws MalformedURLException 
	 * @throws IOException
	 */
	public static String get(String ville,RelType rt) throws IOException 
	{
		String bbox=OSMRequest.getBboxFromVille(ville);
		if(bbox==null)
			return null;
		StringBuilder sb=new StringBuilder();
		sb.append("<osm-script>\n");
		
		if(rt==RelType.MP_OR_ROAD)
		{
			sb.append("<union into=\"_\">\r\n" + 
					"<query into=\"_\" type=\"relation\">\r\n" + 
					"<has-kv k=\"type\" v=\"multipolygon\"/>\n");
			sb.append(bbox);
			sb.append("</query>\r\n" + 
					"<query into=\"_\" type=\"relation\">\r\n" + 
					"<has-kv k=\"route\" v=\"road\"/>\n");
			sb.append(bbox);
			sb.append("</query>\r\n" + 
					"</union>\n");
		}
		else if(rt==RelType.MULTIPOLYGON)
		{
			sb.append("<query into=\"_\" type=\"relation\">\r\n" + 
					"<has-kv k=\"type\" v=\"multipolygon\"/>\n");
			sb.append(bbox);
			sb.append("</query>\n");
		}
		else if(rt==RelType.ROAD)
		{
			sb.append("<query into=\"_\" type=\"relation\">\r\n" + 
					"<has-kv k=\"route\" v=\"road\"/>\n");
			sb.append(bbox);
			sb.append("</query>\n");
		}
		sb.append("<union into=\"_\">\r\n" + 
				"<item from=\"_\" into=\"_\"/>\r\n" + 
				"<recurse from=\"_\" into=\"_\" type=\"down\"/>\r\n" + 
				"</union>\r\n" + 
				"<print />\r\n" + 
				"</osm-script>");
		
		return sb.toString();
	}
	
	/**
	 * TU de la classe
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
//		System.out.println(OSMRequest.getBboxFromVille("Avignon"));
		System.out.println(OSMRequest.get("Avignon",RelType.MP_OR_ROAD));
	}

}
