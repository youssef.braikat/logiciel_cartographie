package fr.univavignon.ceri.deskmap;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;


/**
 * This class is used to launch the application.
 * 
 * @author Diennet Thomas
 * @author Anfosso Tommy
 * @author Chevillard Thibault
 * @author Olivero Antoine
 */
public class Launcher extends Application
{	
	public static TaskHandler<Object> taskHandler=null;
	
	private GridPane grid1,grid2,main;
	private ColumnConstraints c11,c12, c13 ,c3,column1;
	private Button button1,button2,button3;
	private TextArea pathDesc,stateBar;
	private RowConstraints r3,row1,row2;
	private Canvas canvas;
	private SplitPane splitPane;
	private ComboBox<String> cb1, cb2, cb21, cb3, cb31;
	
	private BooleanBinding booleanBind;
	private FilteredList<String> filteredItems;
	
	/**
	 * Sets the the whole UI
	 */
	private void initInterfaceElements()
	{
	////////////////////Creating  grid1////////////////////
	this.grid1 = new GridPane();
	this.c11 = new ColumnConstraints();
	this.c11.setPercentWidth(40);
	this.c11.setHgrow(Priority.ALWAYS);
	this.c12 = new ColumnConstraints();
	this.c12.setPercentWidth(30);
	this.c12.setHgrow(Priority.ALWAYS);
	this.c13 = new ColumnConstraints();
	this.c13.setPercentWidth(30);
	this.c13.setHgrow(Priority.ALWAYS);
	this.grid1.getColumnConstraints().addAll(this.c11, this.c12, this.c13);
	
	//Setting size for the pane  
	this.grid1.setMinSize(300, 300); 
	this.grid1.setPrefSize(450, 450);
	
	//Setting the padding  
	this.grid1.setPadding(new Insets(10, 10, 10, 10)); 
	
	//Setting the vertical and horizontal gaps between the columns 
	this.grid1.setVgap(10); 
	this.grid1.setHgap(10);       
	
	//Setting the Grid alignment 
	this.grid1.setAlignment(Pos.CENTER);
	
	//Creating TextFields        
	this.cb1 = new ComboBox<String>();
	this.cb1.setEditable(true);
//	this.cb1.setValue("Avignon");
	this.cb1.setPromptText("Ville");
	this.cb2 = new ComboBox<String>();
	this.cb2.setEditable(true);
	this.cb2.setPromptText("Rue de départ");
	this.cb21 = new ComboBox<String>();
	this.cb21.setEditable(true);
	this.cb21.setPromptText("N° de départ");
	this.cb3 = new ComboBox<String>(); 
	this.cb3.setEditable(true);
	this.cb3.setPromptText("Rue d'arrivée");
	this.cb31 = new ComboBox<String>();
	this.cb31.setEditable(true);
	this.cb31.setPromptText("N° d'arrivée");
	
	//Setting textFields initial status
	this.cb2.setDisable(true);
	this.cb21.setDisable(true);
	this.cb3.setDisable(true);
	this.cb31.setDisable(true);
	
	//Creating Buttons
	this.button1 = new Button("Ok"); 
	this.button2 = new Button("Recherche");
	this.button3 = new Button("Arrêt"); 
	
	//Setting buttons initial status
	this.button1.setDisable(false);
	this.button2.setDisable(true);
	this.button3.setDisable(true);
	
	//Setting autocompletion
	
	/*ObservableList<String> items = FXCollections.observableArrayList("Avignon", "Paris", "One", "Two", "Three", "Four", "Five", "Six",
            "Seven", "Eight", "Nine", "Ten");*/
	
    //FilteredList<String> filteredItems = new FilteredList<String>(items, p -> true);
    this.filteredItems = new FilteredList<String>(ParseurJson.getCityName());

    this.cb1.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
        final TextField editor = this.cb1.getEditor();
        final String selected = this.cb1.getSelectionModel().getSelectedItem();

        Platform.runLater(() -> {
            if (selected == null || !selected.equals(editor.getText())) {
                this.filteredItems.setPredicate(item -> {
                    if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
                        return true;
                    }
					return false;
                });
            }
        });
    });

    this.cb1.setItems(this.filteredItems);
    this.cb2.setItems(FXCollections.observableArrayList("1", "2"));
    this.cb21.setItems(FXCollections.observableArrayList("3", "4"));
    this.cb3.setItems(FXCollections.observableArrayList("1", "2"));
    this.cb31.setItems(FXCollections.observableArrayList("3", "4"));
	
	//Setting conditions for buttons/textFields to get enabled/disabled
    this.cb1.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
		if(this.cb1.getPromptText().isEmpty()==false){
			this.button1.setDisable(false);
		}
		else if(this.cb1.getPromptText().isEmpty())
			this.button1.setDisable(true);
			this.cb2.setDisable(true);
			this.cb2.setValue(null);
			this.cb21.setDisable(true);
			this.cb21.setValue(null);
			this.cb3.setDisable(true);
			this.cb3.setValue(null);
			this.cb31.setDisable(true);
			this.cb31.setValue(null);
	});
	
	this.booleanBind = this.cb1.valueProperty().isNull()
  .or(this.cb2.valueProperty().isNull())
  .or(this.cb21.valueProperty().isNull())
  .or(this.cb3.valueProperty().isNull())
  .or(this.cb31.valueProperty().isNull());
	this.button2.disableProperty().bind(this.booleanBind);
	
	//affichage automatique des propositions au fur et a mesure
	this.cb1.focusedProperty().addListener((obs, oldValue, newValue) -> {if(this.cb1.isFocused())	this.cb1.show();});
	this.cb2.focusedProperty().addListener((obs, oldValue, newValue) -> {if(this.cb2.isFocused())	this.cb2.show();});
	this.cb21.focusedProperty().addListener((obs, oldValue, newValue) -> {if(this.cb21.isFocused())	this.cb21.show();});
	this.cb3.focusedProperty().addListener((obs, oldValue, newValue) -> {if(this.cb3.isFocused())	this.cb3.show();});
	this.cb31.focusedProperty().addListener((obs, oldValue, newValue) -> {if(this.cb31.isFocused())	this.cb31.show();});
	
	//Creating Text Area
	this.pathDesc = new TextArea();
	this.pathDesc.setEditable(false);
	this.stateBar = new TextArea();
	this.stateBar.setEditable(false);
	
	//Arranging all the nodes in the grid 
	this.grid1.add(this.cb1, 0, 0, 2, 1);    
	this.grid1.add(this.cb2, 0, 1); 
	this.grid1.add(this.cb21, 1, 1);
	this.grid1.add(this.cb3, 0, 2);
	this.grid1.add(this.cb31, 1, 2);
	this.grid1.add(this.button1, 2, 0); 
	this.grid1.add(this.button2, 0, 3);
	this.grid1.add(this.button3, 1, 3);
	this.grid1.add(this.pathDesc, 0, 4, 3, 1);
	
	////////////////////Creating a grid2////////////////////
	this.grid2 = new GridPane();    
	this.grid2.setGridLinesVisible(true);
	
	this.c3 = new ColumnConstraints();
	this.c3.setPercentWidth(100);
	this.c3.setHgrow(Priority.ALWAYS);
	this.r3 = new RowConstraints();
	this.r3.setPercentHeight(100);
	this.r3.setVgrow(Priority.ALWAYS);
	
	this.grid2.getColumnConstraints().addAll(this.c3);
	this.grid2.getRowConstraints().addAll(this.r3);
	
	//Creating the canvas
	this.canvas = new Canvas();
	
	//GraphicsContext gc = canvas.getGraphicsContext2D();
	this.grid2.add(this.canvas, 0, 0);
	
	//Setting size for the pane  
	this.grid2.setMinSize(400, 400); 
	
	////////////////////Creating splitPane////////////////////
	this.splitPane = new SplitPane();
	this.splitPane.getItems().addAll(this.grid1, this.grid2);
	this.splitPane.setDividerPositions(0.3f, 0.6f, 0.9f);
	
	////////////////////Creating mainGrid////////////////////
	this.main = new GridPane(); 
	
	this.column1 = new ColumnConstraints();
	this.column1.setPercentWidth(100);
	this.column1.setHgrow(Priority.ALWAYS);
	this.main.getColumnConstraints().addAll(this.column1);
	
	this.row1 = new RowConstraints();
	this.row1.setPercentHeight(95);
	this.row1.setVgrow(Priority.ALWAYS);
	this.row2 = new RowConstraints();
	this.row2.setPercentHeight(5);
	this.row2.setVgrow(Priority.ALWAYS);
	this.main.getRowConstraints().addAll(this.row1, this.row2);
	
	this.main.setPrefSize(1450, 400); 
	
	//Setting the padding  
	this.main.setPadding(new Insets(5, 5, 5, 5)); 
	
	//Setting the vertical and horizontal gaps between the columns 
	this.main.setVgap(0); 
	this.main.setHgap(0);       
	
	//Setting the Grid alignment 
	this.main.setAlignment(Pos.CENTER);
	
	//Arranging all the nodes in the grid 
	this.main.add(this.splitPane, 0, 0);    
	this.main.add(this.stateBar, 0, 1);
	
	}

	static public void griserElements(Collection<Node> nodes,boolean value)
	{
		if(nodes!=null)
		{
			for(Node n : nodes) {
				if(n!=null) {
					if(n instanceof Pane)
						Launcher.griserElements(((Pane) n).getChildren(), value);
					else
						n.setDisable(value);
				}
			}
		}
	}
	
	/**
	 * Fonction lancée lorsque le btn ville est cliquée
	 * @param event
	 */
	public void btnVille_Click(ActionEvent event){
		if(!this.filteredItems.contains(this.cb1.getValue()))
		{
			Launcher.taskHandler.getTask().updateMessageFromOut("Erreur : Nom de ville incorrect !");
			this.cb1.setStyle("-fx-border-color:red");
		}	
		else
		{
//			this.pathDesc.setText(this.textField1.getText());
			this.cb1.setStyle("");
			Launcher.taskHandler.getTask().updateMessageFromOut("Ville "+this.cb1.getValue().toString()+" choisie.");
			this.button1.setDisable(true);
			this.cb2.setDisable(false);
			this.cb21.setDisable(false);
			this.cb3.setDisable(false);
			this.cb31.setDisable(false);
		}	
	}
	
	/**
	 * fonction lancée lorsque le btn rechercher est cliqué
	 * @param event
	 * @throws Exception
	 */
	public void btnRechercher_Click(ActionEvent event) throws Exception {
		//je créer une nouvelle tâche recherche et je bind les messages de la tâche à la barre d'info de l'interface
		Launcher.taskHandler.setTask(new Task_Recherche<>(this.button2, this.button3, this.cb1,this.pathDesc,this.booleanBind,this.grid1));
	    this.stateBar.textProperty().bind(Launcher.taskHandler.getTask().messageProperty());
//	  	System.out.println(Launcher.taskHandler.getTask().isRunning()?"ca tourne":"ca tourne pas\n");
	  		
		if(!Launcher.taskHandler.getRunning())
		{
			//tout désactiver sauf bouton 'annuler'
			this.button2.disableProperty().unbind();
			Launcher.griserElements(this.grid1.getChildren(), true);
			this.button3.setDisable(false);
			
			Thread t=new Thread(Launcher.taskHandler.getTask());
			t.setDaemon(true);
			t.start();
		}
	}
	
	/**
	 * fonction lancée lorsque le btn annuler est cliqué
	 * @param event
	 * @throws Exception
	 */
	public void btnAnnuler_Click(ActionEvent event) throws Exception {
		if(Launcher.taskHandler.getTask()!=null) {
			Launcher.taskHandler.getTask().cancel();
			Launcher.taskHandler.getTask().updateMessageFromOut("Annulation de la recherche...");
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		// TODO Auto-generated method stub
		
	    this.initInterfaceElements();
	    
	  //initialisation du task handler avec la tache Recherche, executée dans un thread à part lors de l'appui du bouton Recherche
	    Launcher.taskHandler=new TaskHandler<>(new Task_Recherche<>(this.button2, this.button3, this.cb1,this.pathDesc,this.booleanBind,this.grid1));
	    
	    //je bind les messages d'informations de la tache du task handler
	    this.stateBar.textProperty().bind(Launcher.taskHandler.getTask().messageProperty());
	    
	    //valeur initiale du message d'info
	    Launcher.taskHandler.getTask().updateMessageFromOut("En attente d'une ville...");
	    
	    this.button1.setOnAction(this::btnVille_Click);
	    this.button2.setOnAction(event -> {
			try {
				btnRechercher_Click(event);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	    this.button3.setOnAction(arg0 -> {
			try {
				btnAnnuler_Click(arg0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		//added for the demo
//		Launcher.griserElements(Arrays.asList(this.cb21,this.cb31), true);
//		Launcher.griserElements(Arrays.asList(this.button2), false);	//ne marche pas a cause du listener tant pis
//		Launcher.taskHandler.getTask().updateMessageFromOut("Ville "+this.cb1.getValue().toString()+" choisie.");
	    
	    
		Scene scene = new Scene(this.main);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Deskmap");
		primaryStage.show();
	}
	
	/**
	 * Launches the game.
	 * 
	 * @param args
	 * 		Not used here.
	 */
	public static void main(String[] args)
	{	//TODO to be completed
		
		//verifie si le répertoire .cache et .pers existent, sinon les creer
		  File file=new File(System.getProperty("user.dir")+File.separator+".cache");
		  if(!file.exists())
			  file.mkdir();
//		  file=new File(System.getProperty("user.dir")+File.separator+".pers");
//		  if(!file.exists())
//			  file.mkdir();
		
		//lance l'appli
		launch(args);
		
		//S'execute lorsque l'application se termine..
		
		//vider le cache
		Cache.clear();
		
		System.out.println("terminated");
	}
}
