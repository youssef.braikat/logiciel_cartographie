package fr.univavignon.ceri.deskmap;

import javafx.concurrent.Task;

/**
 * Classe qui permet de gérer une tâche, savoir si elle est en train d'être executée ou pas.
 * @author Motyak
 * @param <V>
 *
 */
public class TaskHandler<V> {

	private TaskWrapper<V> task=null;
	private boolean running=false;
	
	public TaskHandler(TaskWrapper<V> task) {this.task=task;}
	
	public void setTask(TaskWrapper<V> task){this.task=task;}
	public TaskWrapper<V> getTask(){return this.task;}
	public void setRunning(boolean value){this.running=value;}
	public boolean getRunning(){return this.running;}
}
