package fr.univavignon.ceri.deskmap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//Pour le TU
import java.security.NoSuchAlgorithmException;

/**
 * @author Motyak
 *
 */
public class PersiFichierText {
	
	public PersiFichierText() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Lis le contenu d'un fichier et le retourne en tant que String
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static String persiToString(String path) throws IOException
	{
		InputStream is = new FileInputStream(path);
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
 
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();

		while(line != null){
		   sb.append(line).append("\n");
		   line = buf.readLine();
		}
		
		buf.close();
		
		sb.setLength(sb.length() - 1);	//Permet de retirer le dernier \n, et donc garder exactement le meme contenu
		
		return sb.toString();
	}
	
	public static int persiNbLines(String path) throws IOException
	{
		int nbLines=0;
		InputStream is = new FileInputStream(path);
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		while(buf.readLine()!=null)
			nbLines++;
		buf.close();
		return nbLines;
	}
	
	public static String persiOverview(String path,int n) throws IOException
	{
		int nbLines=PersiFichierText.persiNbLines(path);
		
		if(nbLines<=2*n)
			return PersiFichierText.persiToString(path);
		
		InputStream is = new FileInputStream(path);
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		int i=1;
		
		for(;i<=n;++i)
			sb.append(buf.readLine()).append("\n");
		sb.append("...\n");
		for(;i<=nbLines-n;++i)
			buf.readLine();
		for(;i<=nbLines;++i)
			sb.append(buf.readLine()).append("\n");
		
		buf.close();
		
		sb.setLength(sb.length() - 1);
		
		return sb.toString();

	}
	
	/**
	 * Ecrit le contenu du String dans un fichier
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	public static void stringToPersi(String content,String path) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(content);
	    writer.close();
	}
	
	/**
	 * Renomme le nom d'un fichier (physiquement)
	 * @param dirPath
	 * @param name
	 * @param newName
	 * @param overwrite
	 * @throws Exception
	 */
	public static void rename(String dirPath,String name,String newName,boolean overwrite) throws Exception
	{
		Path source = Paths.get(dirPath + name);
		File file=new File(dirPath + newName);
		if(file.exists())
		{
			if(overwrite)
				file.delete();
			else
				throw new Exception("Renommage : Le nom de fichier est déjà pris.");
		}
		Files.move(source,source.resolveSibling(newName));
	}
	
	/**
	 * TU de la classe
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		String userDir=System.getProperty("user.dir");
		String fsep=File.separator;
		
		String str=new String("contenu\ncontenu2");
		
		//creation du fichier 'path' avec comme contenu 'contenu'
		PersiFichierText.stringToPersi(str, userDir + fsep + ".cache" + fsep + "fichier1");
		
		//renommage de 'fichier1' en 'fichier2'
		PersiFichierText.rename(userDir+fsep+".cache"+fsep, "fichier1", "fichier2", true);
		
		//lecture et affichage du contenu du fichier 'path'
		System.out.println(PersiFichierText.persiToString(userDir + fsep + ".cache" + fsep + "fichier2"));
		
//		String file=Controleur.sendReqAndCacheRes(OSMRequest.get("Avignon", RelType.ROAD));
//		String overview=PersiFichierText.persiOverview(file, 10);
//		System.out.println("\n\n\n"+overview);
	}
}
