package fr.univavignon.ceri.deskmap;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.util.ArrayList;

/**
 * La classe qui gere le parcours et la recuperation de données d'un fichier XML recupéré depuis OSM.
 * 
 * @author Thibaud CHEVILLARD
 */
public class Parser
{
	/**
	 * variable racine qui contient tous les noeuds du fichiers, definie dans la methode defineRacine.
	 */
	private static Element racine = null;
	
	/**
	 * methode qui prend en parametre un fichier XML et genere la racine et la met dans la variable racine.
	 * 
	 * @param file le nom et parcours du fichier XML
	 */
	public static void defineRacine(String file)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try 
	    {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document= builder.parse(new File(file));
			Element rt = document.getDocumentElement();
			racine = rt;
	    }
	    catch (final ParserConfigurationException e) 
	    {
	      e.printStackTrace();
	    }
	    catch (final SAXException e)
	    {
	      e.printStackTrace();
	    }
	    catch (final IOException e) 
	    {
	      e.printStackTrace();
	    }
	}
	
	/**
	 * methode qui sert a obtenir la racine.
	 * 
	 * @return la racine
	 */
	public static Element getRacine()
	{
		return racine;
	}
	
	/**
	 * une methode qui retourne un tableau contenant la latitude et la longitude d'une node en fonction de son id
	 * 
	 * @param id l'id d'un noeud
	 * @return un tableau contenant les coordonnés
	 */
	public static ArrayList<String> getCoordNodeById(String id)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		ArrayList<String> t = new ArrayList<String>();
		for (int i = 0; i<nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "node") 
		    {
				Element node = (Element) racineNoeuds.item(i);
				
				String idt = node.getAttribute("id");

				if(idt.equals(id))
				{
					t.add(node.getAttribute("lat"));
					t.add(node.getAttribute("lon"));
					return t;
				}
		    }
		}
		return null;
	}
	
	/**
	 * Methode parcourant le fichiers pour trouver la way correspondant à l'id rentrés en parametre.
	 * 
	 * @param id l'id d'un noeud
	 * @return les differentes coordonnées composant une routes
	 */
	public static ArrayList<ArrayList<String>> getCoordWayById(String id)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "way") 
		    {
				Element node = (Element) racineNoeuds.item(i);
				
				String idt = node.getAttribute("id");

				if(idt.equals(id))
				{
					NodeList nodeL = node.getChildNodes();

					return parcoursNdWay(nodeL);
				}
		    }
		}
		return null;
	}
	
//	testt
	public static String getNameWayById(String id)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "way") 
		    {
				Element way = (Element) racineNoeuds.item(i);
				
				String idt = way.getAttribute("id");

				if(idt.equals(id))
				{
					NodeList nodes=way.getChildNodes();
					for(int j=0;j<nodes.getLength();j++)
					{
						if(nodes.item(j).getNodeName().equals("tag"))
						{
							Element tag = (Element)nodes.item(j);
							if(tag.getAttribute("k").equals("name"))
							{
								return tag.getAttribute("v");
							}
						}
					}
				}
		    }
		}
		return null;
	}
	
	/**
	 * Methode parcourant les nodes nd d'une way et en retourne les coordonnées de la way
	 * 
	 * @param nodeL Liste des nodes d'une way
	 * @return la liste des coordonnées d'une way
	 */
	public static ArrayList<ArrayList<String>> parcoursNdWay(NodeList nodeL)
	{
		int nbNodeNoeuds = nodeL.getLength();
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();

		boolean pass = false;
		for(int j = 0; j < nbNodeNoeuds; j++)
		{
			if(nodeL.item(j).getNodeName() == "nd") 
		    {
				Element nlt = (Element) nodeL.item(j);
				
				String ref = nlt.getAttribute("ref");

				ArrayList<String> t = getCoordNodeById(ref);
				if(t != null)
				{
					pass = true;
					res.add(t);
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}
	
	/**
	 * une fonction qui parcoure les nodes relation du fichier XML pour trouver celle qui est lié a l'id
	 * 
	 * @param id un string de numero
	 * @return la liste de toutes les listes de coordonnées des nodes
	 */
	/*public static ArrayList<ArrayList<String>> getCoordRelationById2(String id)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName().equals("relation"))
		    {
				Element node = (Element) racineNoeuds.item(i);
				
				String idt = node.getAttribute("id");

				if(idt.equals(id))
				{
					NodeList nodeL = node.getChildNodes();
					
					return parcoursMembersNode(nodeL);
				}
		    }
		}
		return null;
	}*/
	
	/**
	 * une fonction qui parcoure les nodes relation du fichier XML pour trouver celle qui est lié a l'id
	 * 
	 * @param id un string de numero
	 * @return la liste de toutes les listes de coordonnées des way
	 */
	public static ArrayList<ArrayList<ArrayList<String>>> getCoordRelationById(String id)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName().equals("relation"))
		    {
				Element node = (Element) racineNoeuds.item(i);
				
				String idt = node.getAttribute("id");

				if(idt.equals(id))
				{
					NodeList nodeL = node.getChildNodes();
					
					return getCoordRelation(nodeL);
				}
		    }
		}
		return null;
	}
	
	/**
	 * une fonction qui recupere les coordonnées des way compris dans les membres d'une relation
	 * 
	 * @param nodeL la liste des members d'une node relation
	 * @return la liste de toutes les listes de coordonnées des way
	 */
	public static ArrayList<ArrayList<ArrayList<String>>> parcoursMembersWay(NodeList nodeL)
	{
		int nbNodeNoeuds = nodeL.getLength();
		ArrayList<ArrayList<ArrayList<String>>> res = new ArrayList<ArrayList<ArrayList<String>>>();

		boolean pass = false;
		for(int j = 0; j < nbNodeNoeuds; j++)
		{
			if(nodeL.item(j).getNodeName() == "member")
		    {
				Element nlt = (Element) nodeL.item(j);
				
				if(nlt.getAttribute("type").equals("way"))
				{
					String ref = nlt.getAttribute("ref");

					ArrayList<ArrayList<String>> t = getCoordWayById(ref);
					
					if(t != null)
					{
						res.add(t);
						pass = true;
					}
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}
	
	/**
	 * une fonction qui recupere les coordonnées des nodes compris dans les membres d'une relation
	 * 
	 * @param nodeL la liste des membres d'une relation
	 * @return la liste des coordonnées des nodes
	 */
	public static ArrayList<ArrayList<String>> parcoursMembersNode(NodeList nodeL)
	{
		int nbNodeNoeuds = nodeL.getLength();
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();

		boolean pass = false;
		for(int j = 0; j < nbNodeNoeuds; j++)
		{
			if(nodeL.item(j).getNodeName() == "member") 
		    {
				Element nlt = (Element) nodeL.item(j);
				
				if(nlt.getAttribute("type").equals("node"))
				{
					String ref = nlt.getAttribute("ref");

					ArrayList<String> t = getCoordNodeById(ref);

					if(t != null)
					{
						res.add(t);
						pass = true;
					}
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}	
	
	/**
	 * methode servant a trouvé les batiments parmis les nodes, affiche leur noms (si il y en a un) et l'id de la node
	 */
	public static void findBuilding()
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for (int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "relation" || racineNoeuds.item(i).getNodeName() == "way")
			{
				NodeList nodeL = racineNoeuds.item(i).getChildNodes();
				int nbRN = nodeL.getLength();				
				
				int building = 0;
				
				for (int j = 0; j < nbRN; j++) 
				{
					if(nodeL.item(j).getNodeName() == "tag")
					{
						Element node = (Element) nodeL.item(j);

						if(node.getAttribute("k").equals("building"))
						{
							building = 1;
						}
					}
				}
				if(building == 1)
				{
					for (int j = 0; j < nbRN; j++) 
					{
						if(nodeL.item(j).getNodeName() == "tag")
						{
							Element node = (Element) nodeL.item(j);
							Element nt = (Element) racineNoeuds.item(i);
							
							if(node.getAttribute("k").equals("name"))
							{
								Element nt2 = (Element) ((NodeList) nt).item(j);
								System.out.println("id : " + nt.getAttribute("id") + "\t nom : " + nt2.getAttribute("v"));
							}
							else
							{
								System.out.println("id : " + nt.getAttribute("id"));
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * une fonction qui sert a recuperer les coordonnées des limites de la zone compris dans le fichier
	 * 
	 * @return les coordonnées des limites de la zones recupéré
	 */
	public static ArrayList<String> getBounds()
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for (int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "bounds")
			{
				ArrayList<String> res = new ArrayList<String>();
				
				Element node = (Element) racineNoeuds.item(i);
				
				String t1 = node.getAttribute("minlat");
				String t2 = node.getAttribute("minlon");
				String t3 = node.getAttribute("maxlat");
				String t4 = node.getAttribute("maxlon");
				
				res.add(t1);
				res.add(t2);
				res.add(t3);
				res.add(t4);
				
				return res;
			}
		}
		return null;
	}
	
	/**
	 * fonction qui cherche parmis les Relation celle qui correspond au nom rentrés en argument,
	 * si ne trouve pas parmis les Realtions cherche a dans les Way
	 * 
	 * @param name un string qui contient le nom complet d'un element
	 * @return la liste des coordonnées des different points qui compose la relation
	 */
	public static ArrayList<ArrayList<ArrayList<String>>> getCoordRelationByName(String name)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "relation")
		    {
				Element node = (Element) racineNoeuds.item(i);
				NodeList nodeL = node.getChildNodes();
				int t = nodeL.getLength();
				
				for(int j = 0; j < t; j++) 
				{
					if(nodeL.item(j).getNodeName() == "tag")
					{
						Element n = (Element) nodeL.item(j);
						if(n.getAttribute("k").equals("name"))
						{
							if(n.getAttribute("v").equals(name))
							{
								return getCoordRelation(nodeL);
							}
						}
					}
				}
		    }
		}
		return getCoordWayByName(name);
	}
	
	/**
	 * fonction qui cherche les coordonnées des way et des nodes qui compose la Relation
	 * 
	 * @param nodeL la liste des nodes d'une Relation
	 * @return la liste des coordonnées des different points qui compose la relation
	 */
	public static ArrayList<ArrayList<ArrayList<String>>> getCoordRelation(NodeList nodeL)
	{		
		ArrayList<ArrayList<ArrayList<String>>> res = parcoursMembersWay(nodeL);
		if(res == null)
		{
			res = new ArrayList<ArrayList<ArrayList<String>>>();

			ArrayList<ArrayList<String>> t = parcoursMembersNode(nodeL);
			if(t == null)
			{
				return null;
			}
			res.add(t);
		}
		return res;
	}
	
	/**
	 * fonction qui cherche parmis les Way celle qui correspond au nom rentrés en argument
	 * 
	 * @param name un string qui contient le nom complet d'un element
	 * @return la liste des coordonnées des different points qui compose la way
	 */
	public static ArrayList<ArrayList<ArrayList<String>>> getCoordWayByName(String name)
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		ArrayList<ArrayList<ArrayList<String>>> res = new ArrayList<ArrayList<ArrayList<String>>>();
		boolean pass = false;
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "way") 
		    {
				Element node = (Element) racineNoeuds.item(i);
				NodeList nodeL = node.getChildNodes();
				
				int t = nodeL.getLength();
				
				for(int j = 0; j < t; j++) 
				{
					if(nodeL.item(j).getNodeName() == "tag")
					{
						Element n = (Element) nodeL.item(j);
						if(n.getAttribute("k").equals("name"))
						{
							if(n.getAttribute("v").equals(name))
							{
								pass = true;
								res.add(getCoordWayById(node.getAttribute("id")));
							}
						}
					}
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}
	
	/**
	 * une fonction qui cherche la liste des area parmis les way
	 * 
	 * @return  liste des id des way qui sont des area
	 */
	public static ArrayList<String> getAreaWay()
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		
		ArrayList<String> res = new ArrayList<String>();
		boolean pass = false;
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "way") 
		    {
				Element node = (Element) racineNoeuds.item(i);
				NodeList nodeL = node.getChildNodes();

				Element n = (Element) nodeL.item(1);
				int nbrn = nodeL.getLength();
				for(int j = 4; j < nbrn-2; j++)
				{
					if(nodeL.item(j-2).getNodeName() == "nd" && nodeL.item(j).getNodeName() == "tag")
					{
						Element m = (Element) nodeL.item(j-2);
						
						if(n.getAttribute("ref").equals(m.getAttribute("ref")))
						{
							res.add(node.getAttribute("id"));
							pass = true;
							
						}
					}
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}
	
	/**
	 * une fonction qui cheche les routes parmis les nodes Way
	 * 
	 * @return uen liste d'id
	 */
	public static ArrayList<String> getHighway()
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		
		ArrayList<String> res = new ArrayList<String>();
		boolean pass = false;
		for(int i = 0; i < nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeName() == "way") 
		    {
				Element node = (Element) racineNoeuds.item(i);
				NodeList nodeL = node.getChildNodes();

				int nbrn = nodeL.getLength();
				for(int j = 0; j < nbrn; j++)
				{
					if(nodeL.item(j).getNodeName() == "tag") 
				    {
						Element n = (Element) nodeL.item(j);
						if(n.getAttribute("k").equals("highway"))
						{
							ArrayList<String> temp = getAreaWay();
							if(temp != null)
							{
								boolean pass2 = true;
								for(int k = 0; k < temp.size(); k++)
								{
									if(node.getAttribute("id").equals(temp.get(k)))
									{
										pass2 = false;
									}
								}
								if(pass2 == true)
								{
									res.add(node.getAttribute("id"));
									pass = true;
								}
							}
						}
				    }
				}
		    }
		}
		if(pass == false)
		{
			return null;
		}
		return res;
	}
	
	public static ArrayList<ArrayList<ArrayList<String>>> getCoordAllHighway()
	{
		ArrayList<String> idList = getHighway();
		
		ArrayList<ArrayList<ArrayList<String>>> ret = new ArrayList<ArrayList<ArrayList<String>>>();
		if(idList.size() == 0)
		{
			return null;
		}
		
		for(int i = 0; i < idList.size(); i++)
		{
			String id = idList.get(i);
			ret.add(getCoordWayById(id));
		}
		return ret;
	}
	
	/**
	 * Fonction de test
	 * affiche toutes les nodes principales ainsi que leur ID.
	 */
	public static void affAllNode()
	{
		NodeList racineNoeuds = racine.getChildNodes();
		int nbRacineNoeuds = racineNoeuds.getLength();
		for (int i = 0; i<nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
		    {
				Element node = (Element) racineNoeuds.item(i);

				System.out.println("name : " + node.getNodeName() + "\t id : " + node.getAttribute("id"));
		    }				
		}
	}
	
	/**
	 * Fonction de test
	 * affiche le contenu d'un ArrayList<String> rentré en parametre.
	 * 
	 * @param t1 ArrayList<String>
	 */
	public static void affAS(ArrayList<String> t1)
	{
		if(t1 != null)
		{
			for(int i = 0; i < t1.size(); i++)
			{
				System.out.println(t1.get(i));
			}
		}
	}
	
	/**
	 * Fonction de test
	 * affiche le contenu d'un ArrayList<ArrayList<String>> rentré en parametre.
	 * 
	 * @param t1 ArrayList<ArrayList<String>>
	 */
	public static void affAAS(ArrayList<ArrayList<String>> t1)
	{
		if(t1 != null)
		{
			for(int i = 0; i < t1.size(); i++)
			{
				ArrayList<String> t2 = t1.get(i);
				if(t2 != null)
				{
					for(int j = 0; j < t2.size(); j++)
					{
						System.out.print(t2.get(j) + "\t");
							
						if(j == t2.size()-1)
						{
							System.out.println();
						}
					}
				}
			}
		}
	}
	
	/**
	 * Fonction de test
	 * affiche le contenu d'un ArrayList<ArrayList<ArrayList<String>>> rentré en parametre.
	 * 
	 * @param t1 ArrayList<ArrayList<ArrayList<String>>>
	 */
	public static void affAAAS(ArrayList<ArrayList<ArrayList<String>>> t1)
	{
		if(t1 != null)
		{
			for(int i = 0; i < t1.size(); i++)
			{
				ArrayList<ArrayList<String>> t2 = t1.get(i);
				if(t2 != null)
				{
					for(int j = 0; j < t2.size(); j++)
					{
						ArrayList<String> t3 = t2.get(j);
						if(t3 != null)
						{
							for(int k = 0; k < t3.size(); k++)
							{
								System.out.print(t3.get(k) + "\t");
									
								if(k == t3.size()-1)
								{
									System.out.println();
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Main servant pour les tests
	 * 
	 * @param args les arguments potentiels
	 */
	public static void main(String[] args)
	{
		String file = "testMap.xml";
		
		defineRacine(file);
		//affAllId();
		
		//String id = "107798735";
		//affAS(getCoordNodeById(id));

		//affAAS(getCoordWayById(id));
		
		//findBuilding();
		

		//String id = "1253994";
		//String id = "1254001";
		//affAAS(getCoordRelationById2(id));
		
		affAS(getBounds());
		System.out.println("--------------------------------------");
		
		//String name = "Rue de Rivoli";
		//String name = "Rue de Lobau";	
		//String name = "Hôtel de Ville (Métro 1)";
		//affAAAS(getCoordRelationByName(name));
		
		//affAS(getAreaWay());
		//affAS(getHighway());
		affAAAS(getCoordAllHighway());
	}
}













