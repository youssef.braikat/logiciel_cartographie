package fr.univavignon.ceri.deskmap;

import javafx.concurrent.Task;

/**
 * @author Motyak
 * @param <V>
 *
 */
public abstract class TaskWrapper<V> extends Task<V> {

	@Override
	protected abstract V call() throws Exception;

	public void updateMessageFromOut(String msg)
	{
		this.updateMessage(msg);
	}
}
