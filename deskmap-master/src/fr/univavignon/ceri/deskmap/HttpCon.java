package fr.univavignon.ceri.deskmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

/**
 * @author Motyak
 *
 */
public class HttpCon {

	/**
	 * Map permettant de faire la conversion enum -> String
	 */
	private static final String[] map = {"GET", "POST"};
	
	public HttpCon() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Permet d'envoyer une requete HTTP, renvoie null si tâche recherche a été annulée en pleine execution
	 * @param typeReq
	 * @param apiUrl
	 * @param headerParams
	 * @param data
	 * @return
	 * @throws MalformedURLException,IOException 
	 * @throws IOException
	 */
	public static String request(Type typeReq,String apiUrl, String[] headerParams,String data) throws IOException
	{
		URL url = new URL(apiUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setRequestMethod(HttpCon.map[typeReq.getValue()]);
		
		if(headerParams!=null) {
			String field,value;
			for(String s : headerParams) {
				if(s!=null) {
					field=s.substring(0, s.indexOf(':'));
					value=s.substring(s.indexOf(':')+1);
					connection.setRequestProperty(field,value);
				}
			}
		}
			
		if(typeReq==Type.POST && data!=null)
		{
			//faire l'envoi des données
			connection.setDoOutput(true);
			OutputStream outputStream = connection.getOutputStream();
			byte[] b = data.getBytes("UTF-8");
			outputStream.write(b);
			outputStream.flush();
			outputStream.close();
		}
		
		System.out.println("Stockage de la réponse..");
		// To store our response
		StringBuilder content;

		// Get the input stream of the connection
		try (BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
			String line;
			content = new StringBuilder();
			while ((line = input.readLine()) != null) {
				if(Launcher.taskHandler!=null && Launcher.taskHandler.getTask()!=null && Launcher.taskHandler.getTask().isCancelled())
				{
					Launcher.taskHandler.getTask().updateMessageFromOut("Recherche annulée.");
					Launcher.taskHandler.setRunning(false);
					return null;
				}
					
				
				// Append each line of the response and separate them
				content.append(line);
				content.append(System.lineSeparator());
			}
		} 
		finally {
			connection.disconnect();
		}	
				
		System.out.println("url : " + connection.getURL() + "\n");
		System.out.println("type de requete : " + connection.getRequestMethod() + "\n");
		System.out.println("paramètres du header : ");
		for (String header : connection.getHeaderFields().keySet()) {
			if (header != null) {
				for (String value : connection.getHeaderFields().get(header))
					System.out.println(header + ":" + value);
			}
		}
		
		return content.toString();
	}
	
	/**
	 * TU de la classe
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		String res=HttpCon.request(
			Type.POST, 
			Controleur.OVERPASS_API_URL, 
			new String[] {"Content-Type: text/xml", "Accept: text/xml"}, 
			new String("<osm-script timeout=\"900\" element-limit=\"1073741824\">\r\n" + 
				"  <bbox-query s=\"51.15\" w=\"7.0\" n=\"51.16\" e=\"7.01\"/>\r\n" + 
				"  <print/>\r\n" + 
				"</osm-script>"));
		
		System.out.println("\n"+res);
		
//		String res=HttpCon.request(
//				Type.POST, 
//				"https://lz4.overpass-api.de/api/interpreter", 
//				new String[] {"Content-Type: text/xml", "Accept: text/xml"}, 
//				OSMRequest.get("Avignon",RelType.ROAD));
//				
//		
//		PersiFichierText.stringToPersi(res, Cache.DIR_PATH + "test");
	}
}
